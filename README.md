# Matrix POSIX SDK

A POSIX shell compliant [\[matrix\]](https://matrix.org/) client server API implementation.

> Your [matrix] never felt that geeky.

## Scope

The scope of this project is definitely to become compatible to original `sh(1)` from May 1989 - around 35 years ago,
though, current state is not yet compatible. It currently runs on `busybox(1)` shipped in current UNIXoide systems.

Please don't complain about missing `sh(1)` compliance in issues, but feel free to discuss options and ideas of how
to get compatible to even earlier versions.

A brief history about the different original `sh(1)` implementations and its descendants can be found here:
https://www.in-ulm.de/~mascheck/various/ash/

## Dependencies

- POSIX compliant Shell implementation in env
- /usr/bin/env available
- jq
- curl

### In man entries

- `busybox(1)`
- `env(1)`
- `tr(1)`
- `curl(1)`
- `jq(1)`

## Usage

```shell
# ensure you start dash, most systems link `sh` to `bash`
env dash

export MATRIX_POSIX_SDK=/path/to/matrix-posix-sdk/repo
. "${MATRIX_POSIX_SDK}/matrix.sh"

Matrix myClient

# check a well-known entry for a homeserver
myClient_getWellKnown conduit.rs

# or directly set the homeserver URL
myClient_setHomeserver "https://conduit.koesters.xyz"

# get and store the supported API versions from the server
myClient_versions

myClient_homeserver
# https://conduit.koesters.xyz

# store your session
echo "$myClient_config" > myClient.mps.json

# restore your session
myClient_config="$(cat myClient.mps.json)"
```

## State of the project

The project is pre-alpha, a usage study whether a high-level implementation of a complex standard like \[matrix\] is
actually possible using the limited feature set provided by a bare UNIX environment.

## Feature support
- [x] Basic concept of object-oriented client bootstrap
- [x] State management
- [x] Initial endpoints
- [ ] User-related endpoints
- [ ] Room endpoints
- [ ] Sync loop
- [ ] libolm FFI