#!/usr/bin/env sh

if [ -z $MATRIX_POSIX_SDK ]; then
  echo "Please ensure the MATRIX_POSIX_SDK environment variable is set."
  exit 1
fi

if [ -z "$(which "jq")" ]; then
  echo "Please ensure jq is installed and available in PATH."
  exit 1
fi

if [ -z "$(which "curl")" ]; then
  echo "Please ensure curl is installed and available in PATH."
  exit 1
fi

. "$MATRIX_POSIX_SDK/src/api/account.sh"
. "$MATRIX_POSIX_SDK/src/api/discovery.sh"

function Matrix() {
  name=$1

  eval "${name}_config={}"

  eval "function ${name}_getWellKnown () {
    response=\"\$(_getWellKnown $name \$1 \$2)\"

    homeserver=\"\$(echo \"\$response\" | jq '.[\"m.homeserver\"][\"base_url\"]' | tr -d '\"')\"

    configName=\"\${name}_config\"
    config=\"\$(eval echo \"\\\$\${configName}\")\"

    newConfig=\"\$(echo \"\$config\" | jq '. += {\"homeserver\": \"'\${homeserver}'\"}')\"
    eval \"\${configName}='\$newConfig'\"
    export \$configName

    return 0
  }"

  eval "function ${name}_versions () {
    configName=\"\${name}_config\"
    config=\"\$(eval echo \"\\\$\${configName}\")\"

    response=\"\$(_versions \"\$config\")\"

    version=\"\$(echo \"\$response\" | jq -S '.[\"versions\"][-1]' | tr -d '\"')\"

    newConfig=\"\$(echo \"\$config\" | jq '. += {\"supportedVersion\": \"'\${version}'\"}')\"
    eval \"\${configName}='\$newConfig'\"
    export \$configName

    return 0
  }"

  eval "function ${name}_login () {
    configName=\"\${name}_config\"
    config=\"\$(eval echo \"\\\$\${configName}\")\"

    response=\"\$(_login \"\$config\" \"\$1\" \"\$2\")\"

    token=\"\$(echo \"\$response\" | jq '.[\"access_token\"]' | tr -d '\"')\"
    mxid=\"\$(echo \"\$response\" | jq '.[\"user_id\"]' | tr -d '\"')\"

    newConfig=\"\$(echo \"\$config\" | jq '. += {\"accessToken\": \"'\${token}'\"}')\"
    newConfig=\"\$(echo \"\$newConfig\" | jq '. += {\"mxid\": \"'\${mxid}'\"}')\"
    eval \"\${configName}='\$newConfig'\"
    export \$configName

    return 0
  }"

  eval "function ${name}_setHomeserver () {
    if [ -z \"\$1\" ]; then
      return 1
    fi

    configName=\"\${name}_config\"
    config=\"\$(eval echo \"\\\$\${configName}\")\"

    newConfig=\"\$(echo \"\$config\" | jq '. += {\"homeserver\": \"'\$1'\"}')\"
    eval \"\${configName}='\$newConfig'\"
    export \$configName

    return 0
  }"

  eval "function ${name}_homeserver () {
    configName=\"\${name}_config\"
    config=\"\$(eval echo \"\\\$\${configName}\")\"
    echo \"\$config\" | jq '.[\"homeserver\"]' | tr -d '\"'
    return 0
  }"

  eval "function ${name}_accessToken () {
    configName=\"\${name}_config\"
    config=\"\$(eval echo \"\\\$\${configName}\")\"
    echo \"\$config\" | jq '.[\"accessToken\"]' | tr -d '\"'
    return 0
  }"

  eval "function ${name}_mxid () {
    configName=\"\${name}_config\"
    config=\"\$(eval echo \"\\\$\${configName}\")\"
    echo \"\$config\" | jq '.[\"mxid\"]' | tr -d '\"'
    return 0
  }"

}
