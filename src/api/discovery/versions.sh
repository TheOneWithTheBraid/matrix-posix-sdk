#!/usr/bin/env sh

. "$MATRIX_POSIX_SDK/src/utils/matrix_api_call.sh"

function _versions () {
  config=$1

  response="$(_matrix_api_request "$config" "GET" "versions")"

  echo "$response"
  return 0
}
