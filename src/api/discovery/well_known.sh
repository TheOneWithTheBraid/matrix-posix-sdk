#!/usr/bin/env sh

. "$MATRIX_POSIX_SDK/src/utils/matrix_api_call.sh"

function _getWellKnown () {
  client=$1
  homeserver=$2

  if [ -z $homeserver ]; then
    return 1
  fi

  if [ -z $3 ]; then
    protocol="https"
  else
    protocol="$3"
  fi

  response="$(_generic_http_request "GET" "$protocol://$homeserver/.well-known/matrix/client")"

  echo "$response"
  return 0
}
