#!/usr/bin/env sh

. "$MATRIX_POSIX_SDK/src/utils/matrix_api_call.sh"


# TODO: support UIA
function _login () {
  config=$1
  user=$2

  # TODO: escape password
  password=$3

  body="{\"identifier\": {\"type\": \"m.id.user\", \"user\": \"$user\"}, \"password\": \"$password\", \"type\": \"m.login.password\"}"

  response="$(_matrix_api_request "$config" "POST" "v3/login" "$body")"

  echo "$response"
  return 0
}
