#!/usr/bin/env sh

function _generic_http_request () {
  method=$1
  url=$2
  authentication=$3
  body=$4

  if [ -z $authentication ]; then
    if [ -z "$body" ]; then
      response="$(curl --http2 -X "$method" -fs --retry 3 "$url")"
    else
      response="$(curl --http2 -X "$method" --data "$body" -H "Content-Type: application/json" -fs --retry 3 "$url")"
    fi
  else
    if [ -z "$body" ]; then
      response="$(curl --http2 -X "$method" -fs --retry 3 -H "Authentication: Bearer: $authentication" "$url")"
    else
      response="$(curl --http2 -X "$method" --data "$body" -H "Content-Type: application/json" -H "Authentication: Bearer: $authentication" -fs --retry 3 "$url")"
    fi
  fi

  echo "$response"
  return 0
}

function _matrix_api_request () {
  config=$1
  method=$2
  endpoint=$3
  body=$4

  homeserver="$(echo "$config" | jq '.["homeserver"]' | tr -d '"')"
  url="${homeserver}/_matrix/client/${endpoint}"

  authentication="$(echo "$config" | jq '.["accessToken"]' | tr -d '"')"
  if [ "$authentication" == "null" ]; then
    authentication=""
  fi

  response="$(_generic_http_request "$method" "$url" "$authentication" "$body")"

  echo "$response"
  return 0
}
